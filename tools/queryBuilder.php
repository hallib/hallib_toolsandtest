<?php
namespace uga\hallibtt\tools;

/**
 * Formulaire pour la création de requête pour l'API Hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
session_start();

if(isset($_GET['referentiel'])) {
    if($_GET['referentiel']) {
        $page = 'restriction';
    }
} elseif(isset($_GET['instance'])||isset($_GET['noRestriction'])||isset($_GET['collection'])) {
    if(isset($_GET['noRestriction'])) {
        $restriction = '';
    } else {
        $restriction = ($_GET['instance']!='none')?$_GET['instance']:$_GET['collection'];
    }
    $_SESSION['restriction'] = $restriction;
    $page = 'query';
} elseif(isset($_GET['queryValue'])) {
    $query = '';
    $query .= $_GET['queryValue'];
    $_SESSION['query'] = [
        'queryValue' => $_GET['queryValue'],
    ];
    if(isset($_GET['fieldType'])) {
        $query .= $_GET['fieldType'].':';
        $_SESSION['fieldType'] = $_GET['fieldType'];
    }
} else {
    $page = 'referentiel';
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Création de requête Hal</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
<div class="container">
<h1 class="title is-1">Création de requête Hal</h1>
<?php
if($page == 'restriction'):
    // chargement liste des portail
    $dataDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR;
    $portailFile = $dataDir.'instance'.DIRECTORY_SEPARATOR.'list.json';
    $portailData = json_decode(file_get_contents($portailFile));
    $portailList = [];
    foreach($portailData->response->docs as $portail) {
        $portailList[$portail->code] = $portail->name;
    }
    asort($portailList);
    ?>
<h2 class="subtitle is-2">Choix des restriction</h2>
<form>
    <fieldset class="control box columns">
    <div class="column">
        <p>
            <label class="label" for="instance">portail</label>
            <div class="select">
                <select name="instance" id="instance">
                    <option value="none">aucun</option>
                    <?php foreach($portailList as $portailCode => $portailName): ?>
                        <option value="<?= $portailCode ?>"><?= $portailName ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </p>
    </div>
    <div class="column">
    <h3 class="subtitle is-3">collection</h3>
    </div>
    </fieldset>
    <label class="checkbox" for="noRestriction">
        <input type="checkbox" name="noRestriction" id="noRestriction" checked>
        pas de restriction
    </label>
    <p><input class="button" type="submit" value="suivant"></p>
</form>
<?php elseif($page == 'referentiel'): ?>
<h2 class="subtitle is-2">choix du référentiel</h2>
<form>
    <p>
        <label for="referentiel">referentiel</label>
        <div class="select">
            <select name="referentiel" id="referentiel">
                <option value="search">Recherche générale</option>
            </select>
        </div>
    </p>
    <p><input class="button" type="submit" value="suivant"></p>
</form>
<?php elseif($page == 'query'):
    // chargement list des champs
    $dataDir = dirname(__FILE__,2).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR;
    $fieldFile = $dataDir.'search'.DIRECTORY_SEPARATOR.'fields_description.json';
    $fieldData = json_decode(file_get_contents($fieldFile));
?>
<h2 class="subtitle is-2">Construction de la requête principale</h2>
<form>
    <p>
        <label for="field">Champ</label>
        <div class="select">
            <select id="field">
                <option value="">defaut</option>
                <?php foreach(get_object_vars($fieldData) as $key => $value):?>
                    <option value="<?= $key ?>"><?= isset($value->label)?$value->label:$key ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </p>
    <p>
        <label for="fieldType">Type</label>
        <div class="select">
            <select name="fieldType" id="fieldType"></select>
        </div>
    </p>
    <p>
        <label for="queryValue">Valeur rechercher</label>
        <input type="text" name="queryValue" id="queryValue">
    </p>
    <input type="submit" value="suivant">
</form>
<script>
    var fieldsData = null;
    $.ajax({
        url: '../data/search/fields_description.json',
        success: data => {
            fieldsData = data;
        }
    })
    $('#field').on('change', ev => {
        $('#fieldType').empty();
        for(varient of fieldsData[$('#field').val()].variant) {
            $('#fieldType').append($('<option>', {
                value: varient.name,
                text: varient.type
            }))
        }
    })
</script>
<?php endif; ?>
</div>
</body>
</html>