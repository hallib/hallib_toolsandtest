<?php
namespace uga\hallibtt\tools;

$listRef = [
    'anrproject',
    'author',
    'domain',
    'europeanproject',
    'journal',
    'metadatalist',
    'search',
    'structure'
];

foreach($listRef as $referantiel) {
    $dataDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.$referantiel.DIRECTORY_SEPARATOR;
    $descriptionData = json_decode(file_get_contents($dataDir.'fields_description.json'));
    foreach(get_object_vars($descriptionData) as $field) {
        foreach($field->variant as $key => $varant) {
            $field->variant[$key]->currentName = $varant->name;
        }
    }
    file_put_contents($dataDir.'fields_description.json', json_encode($descriptionData, JSON_PRETTY_PRINT));
}