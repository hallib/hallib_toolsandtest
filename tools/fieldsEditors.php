<?php
namespace uga\hallibtt\tools;

/**
 * Utilitaires pour facilité l'édition de la liste des champs d'un référentiel
 * Hal.
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
$dataDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR;
if(isset($_POST['referenciel'])) {
    $dataFile = $dataDir.$_POST['referenciel'].DIRECTORY_SEPARATOR.'fields_description.json';
    file_put_contents($dataFile, json_encode(json_decode($_POST['newData']), JSON_PRETTY_PRINT));
    echo 'ok';
    exit(0);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>éditeur de list d'un champs d'un référentiel Hal</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
<div class="container">
    <h1 class="title is-1">éditeur de list d'un champs d'un référentiel Hal</h1>
    <p>
        <label class="label" for="referenciel">référentiel</label>
        <div class="select">
            <select name="referenciel" id="referenciel">
                <option value="anrproject">Projet ANR</option>
                <option value="author">Auteur</option>
                <option value="domain">Domaine</option>
                <option value="europeanproject">Projets européen</option>
                <option value="journal">Revues</option>
                <option value="search">Recherche général</option>
                <option value="structure">Structure</option>
            </select>
        </div>
    </p>
    <p>
        <label for="field">Champs</label>
        <div class="select">
            <select name="field" id="field"></select>
        </div>
    </p>
    <p>
        <label class="label" for="label">label</label>
        <input class="input" type="text" name="label" id="label">
    </p>
    <p>dynamic : <span id="dynamic"></span></p>
    <p>
        <label class="label" for="description">description</label>
        <textarea class="textarea" name="description" id="description" cols="30" rows="10"></textarea>
    </p>
    <div class="box">
        <p>
            <label for="fieldVariant">variante</label>
            <div class="select">
                <select name="fieldVariant" id="fieldVariant"></select>
            </div>
        </p>
        <p>indexed : <span id="indexed"></span></p>
        <p>stocked : <span id="stocked"></span></p>
        <p>multiplevalue : <span id="multiplevalue"></span></p>
    </div>
    <nav class="navbar is-fixed-bottom">
    <div class="buttons">
        <button id="modify" class="button is-primary">valider</button>
        <button id="sendMod" class="button is-warning">envoyer les modif</button>
    </div>
    </nav>
</div>
</body>
</html>
<script>
    var currentData = null;
    $('#referenciel').on('change', ev => {
        $.ajax({
            url: "../data/" + $('#referenciel').val() + '/fields_description.json',
            success: data => {
                $('#field').empty();
                currentData = data;
                for(fieldValue in data) {
                    fieldLabel = (data[fieldValue].label!=undefined)?data[fieldValue].label:fieldValue;
                    $('#field').append($('<option>', {
                        value: fieldValue,
                        text: fieldLabel
                    }))
                }
            }
        })
    });
    $('#field').on('change', ev => {
        $('#fieldVariant').empty();
        for(fieldVarient of currentData[$('#field').val()].variant) {
            $('#fieldVariant').append($('<option>', {
                value: fieldVarient.name,
                text: fieldVarient.type
            }))
        }
        $('#dynamic').text(currentData[$('#field').val()].dynamic)
        fieldLabel = (currentData[$('#field').val()].label!=undefined)?currentData[$('#field').val()].label:'';
        $('#label').val(fieldLabel);
        $('#description').val(currentData[$('#field').val()].description);
    })
    $('#modify').click(ev => {
        currentData[$('#field').val()].label = $('#label').val();
    })
    $('#sendMod').click(ev => {
        $.ajax({
            type: 'post',
            data: {
                newData: JSON.stringify(currentData),
                referenciel: $('#referenciel').val()
            },
            success: data => {
                console.log(data)
            }
        })
    })
</script>
