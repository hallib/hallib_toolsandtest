<?php
namespace uga\hallibtt\tools;

$dataDir = dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'pretty'.DIRECTORY_SEPARATOR;

$urls = [
    'doctype' => 'https://api.archives-ouvertes.fr/ref/doctype',
];

if(isset($_GET['ref'])&&isset($urls[$_GET['ref']])) {
    $url = $urls[$_GET['ref']];
    $data = json_decode(file_get_contents($url));
    file_put_contents($dataDir.$_GET['ref'].'.json', json_encode($data, JSON_PRETTY_PRINT));
} else {
    echo 'erreur!!!!';
}
