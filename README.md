# hallib_toolsandtest

outil et test pour hallib

## Test unitaire

lancement en ligne de commande :
./vendor/bin/phpunit

(⚠ En cas d'erreur SSL, ce connecter au VPN.)

## Mise à jour des données

Pour metre à jour les liste de données du repertoir data, il suffit de lancé le script updateData :
```
php updateData.php
```
