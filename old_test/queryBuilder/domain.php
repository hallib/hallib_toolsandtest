<?php
namespace uga\hallibtt\test\queryBuilder;
require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\ref\domain\DomainSelector;
use uga\hallib\HTMLGenerator\SelectorGenerator;

//var_dump(uga\hallib\DomainField::getVarientList());

$selectorTest = new DomainSelector();
$selectorTest->extractData();
$selectGenerator = new SelectorGenerator($selectorTest);
$selectGenerator->name = 'domain';
$selectGenerator->label = 'Domaines';
$selectGenerator->frameWorksName = 'bulma';
$codeHtmlFull = $selectGenerator->generateSelectField();
$selectorTest->level = 0;
$codeHtmlL0 = $selectGenerator->generateSelectField();
$selectorTest->selected = ['chim', 'info', 'info.eiah', 'info.info-ai'];
$codeHtmlL0Restricted = $selectGenerator->generateSelectField();
$selectorTest->level = -1;
$codeHtmlRestricted = $selectGenerator->generateSelectField();
$selectGenerator->frameWorksName = 'PlainHTML';
$codePalinHtmlRestricted = $selectGenerator->generateSelectField();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
    <?= $codeHtmlFull ?><br><br>
    <?= $codeHtmlL0 ?><br><br>
    <?= $codeHtmlL0Restricted ?><br><br>
    <?= $codeHtmlRestricted ?><br><br>
    <?= $codePalinHtmlRestricted ?><br><br>
</body>
</html>

