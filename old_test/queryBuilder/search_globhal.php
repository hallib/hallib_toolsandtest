<?
namespace uga\hallibtt\test\queryBuilder;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\ref\structure\StructureField;
use uga\hallib\ref\structure\StructureQuery;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

$query = new SearchQuery([
    'rows' => 10,
    'instence' => "saga",
    'useCursor' => false
]);

$query->baseQuery = new LiteralElement([
    'field' => SearchField::getVarient('collCode_s'),
    'value' => '3S-R'
]);

echo $query->stringValue.'<br>';

$query = new StructureQuery();
$query->addReturnedField('country_s');
$query->addReturnedField('name_s');
$query->baseQuery = new LiteralElement([
    'value' => 253831,
    'field' => StructureField::getVarient('docid')
]);
echo $query->stringValue.'<br>';

$oneDocQuery = new OneDocQuery($query);
var_dump($oneDocQuery->result);
