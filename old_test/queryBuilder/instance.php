<?php
namespace uga\hallibtt\test\queryBuilder;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\ref\instance\InstenceSelector;
use uga\hallib\HTMLGenerator\SelectorGenerator;

$selectorTest = new InstenceSelector();
$selectorTest->extractData();
$selectorTest->sort = false;
$selectGenerator = new SelectorGenerator($selectorTest);
$selectGenerator->label = 'portail';
$selectGenerator->frameWorksName = 'bulma';
$codeHtml = $selectGenerator->generateSelectField();
$selectorTest->sort = true;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portail</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
    <p><?= $codeHtml ?></p>
    <h2 class="subtitle">trier</h2>
    <p><?= $selectGenerator->generateSelectField() ?></p>
</body>
</html>
