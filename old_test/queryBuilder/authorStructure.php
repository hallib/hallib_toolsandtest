<?php
namespace uga\hallibtt\test\authorStructure;

use uga\hallib\OneDocQuery;
use uga\hallib\QueryIterator;
use uga\hallib\ref\authorstructure\AuthorStructureQuery;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';

$q = new AuthorStructureQuery([
    'firstName' => 'serge',
    'lastName' => 'haroche'
]);

echo $q->stringValue;
$qit = new QueryIterator($q);
$r1 = new OneDocQuery($q);
var_dump($r1->result);

foreach($qit as $doc) {
    var_dump($doc);
}
