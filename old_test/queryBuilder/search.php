<?php
namespace uga\hallibtt\old_test\queryBuilder;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

use uga\hallib\QueryIterator;
use uga\hallib\queryDefinition\AlmostLiteralElement;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchQuery;
use uga\hallib\search\SearchField;

session_start();

$test = new LiteralElement([
    'value' => 'test',
    'truncateChar' => '*',
]);

echo $test.'<br>';

$test2 = new AlmostLiteralElement(['value' => 'test']);

echo $test2.'<br>';

$sq = new SearchQuery();
$sq->addReturnedField(SearchField::getVarient('abstract_s'));
$sq->addReturnedField(SearchField::getVarient('title', 'string'));
$sq->addReturnedField(SearchField::getVarient('type', 'string'));
$sq->addReturnedField('label_bibtex');
$interval = new IntervalElement(['minValue' => '2020']);
echo $interval.'<br>';
$sq->addFilter($interval, 'publicationDateY_i');
$sq->baseQuery = new LiteralElement(['value' => 'japan']);
echo new LiteralElement(['value' => 'japan']).'<br>';
echo $sq->stringValue.'<br>';
$sq->useCursor = false;
echo $sq->stringValue.'<br>';

$sq2 = new SearchQuery(['baseQuery' => new LiteralElement(['value' => '*:*', 'escape' => false])]);
$sq2->addReturnedField(SearchField::getVarient('title', 'string'));
$newFilterQuery = new IntervalElement([
    'minValue' => '""',
    'field' => SearchField::getVarient('doiId_s'),
    'prefix' => '-',
]);
$sq2->addFilterQuery($newFilterQuery);
echo $sq2->stringValue.'<br>';
$qi = new QueryIterator($sq);

// var_dump(SearchQuery::getFieldClass()::getVarientList());

/*foreach($qi as $doc) {
    var_dump($doc->abstract_s);
    var_dump($doc->title_s);
    var_dump($doc->type_s);
    var_dump($doc->json);
}*/

$sqi = new SearchQuery();
$sqi->addReturnedField(SearchField::getVarient('abstract', 'string'));
$sqi->addReturnedField(SearchField::getVarient('title_s'));
$sqi->addReturnedField(SearchField::getVarient('type', 'string'));
$sqi->baseQuery = new LiteralElement([
    'value' => '3S-R',
    'field' => SearchField::getVarient('structure', 'text')
]);
$sqi->useCursor = true;
$qii = new QueryIterator($sqi);

$structureField = SearchField::getVarient('structure_t');

$sqs = new SearchQuery();
$sqs->baseQuery = new LiteralElement([
    'value' => '*'
]);
$sqs->addFilterQuery(new LiteralElement([
    'value' => 'ART',
    'field' => SearchField::getVarient('docType_s'),
]));
$sqs->instance = 'saga';

$serialized = serialize($sq2);
echo $serialized;
var_dump(unserialize($serialized));

if(!isset($_SESSION['sq2'])) {
    $_SESSION['sq2'] = $sq2;
} else {
    $qii = new QueryIterator($_SESSION['sq2']);
    /*
    foreach($qii as $doc) {
        var_dump($doc);
    }*/
}

$sqs->addFilterQuery(new IntervalElement([
    'minValue' => '""',
    'field' => SearchField::getVarient('doiId_s'),
    'prefix' => '-',
]));
echo $sqs->stringValue.'<br>';
$sqs->collection = 'FRANCE-GRILLES';
echo $sqs->stringValue.'<br>';
$sqs->instance = 'saga';
echo $sqs->stringValue.'<br>';

$sq0 = new SearchQuery([
    'rows' => 10,
    'baseQuery' => new LiteralElement([
        'value' => '3S-R',
        'field' => SearchField::getVarient('structure_t'),
    ]),
    'useCursor' => true,
]);
$sq0->addFilterQuery(new LiteralElement([
    'value' => 'ART',
    'field' => SearchField::getVarient('docType_s'),
]));
$sq0->addFilterQuery($newFilterQuery);
$sq0->addFilterQuery($newFilterQuery);
$qi0 = new QueryIterator($sq0);
/*foreach($qi0 as $doc) {
    var_dump($doc);
}*/

$sq1 = new SearchQuery([
    'rows' => 10,
    'baseQuery' => new LiteralElement([
        'value' => 'tralala',
        'field' => SearchField::getVarient('structure_t'),
    ]),
    'useCursor' => true,
]);
$qi1 = new QueryIterator($sq1);
foreach($qi1 as $doc) {
    var_dump($doc);
}

/*
var_dump(uga\hallib\SearchField::getList());
*/

$sqn = new SearchQuery([
    'rows' => 10,
    'baseQuery' => new LiteralElement([
        'value' => '3S-R',
        'field' => SearchField::getVarient('structure_t'),
    ]),
    'useCursor' => false
]);
$sqn->addReturnedField(SearchField::getVarient('domainAllCode_s'));
$qin = new QueryIterator($sqn);
var_dump($sqn->stringValue);
foreach($qin as $doc) {
    var_dump($doc);
    if (is_array($doc->domainAllCode_s)) foreach($doc->domainAllCode_s as $domain)
    echo $domain.'<br>';
}

echo '<h2>test Sort</h2>';

$sqSort = new SearchQuery([
    'baseQuery' => new LiteralElement(['value' => '*:*', 'escape' => false]),
    'rows' => 2,
]);
$sortVarient = SearchField::getVarient('producedDateY_i');
$sqSort->addReturnedField($sortVarient);
$sqSort->addReturnedField(SearchField::getVarient('uri_s'));
$sqSort->sort = $sortVarient;
echo $sqSort->stringValue.'<br>';
$qiSort = new QueryIterator($sqSort);
foreach($qiSort as $doc) {
    var_dump($doc).'<br>';
}
