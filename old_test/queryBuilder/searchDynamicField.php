<?php
namespace uga\hallibtt\test\queryBuilder;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

use uga\hallib\search\SearchQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchField;
use uga\hallib\QueryIterator;

$dynFields = ['level0_domain_s', 'level1_domain_s', 'level2_domain_s', 'level3_domain_s'];

$sq1 = new SearchQuery([
    'rows' => 10,
    'baseQuery' => new LiteralElement([
        'value' => '*:*'
    ]),
]);
echo $sq1->stringValue;
$sq1->addReturnedField(SearchField::getVarient('abstract_s'));

foreach($dynFields as $dynField) {
    $sq1->addReturnedField(SearchField::getVarient($dynField));
    var_dump(SearchField::getVarient($dynField));
}

$qi1 = new QueryIterator($sq1);

foreach($qi1 as $doc) {
    var_dump($doc);
}
