<?php
namespace uga\hallibtt\test\queryBuilder;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\ref\structure\StructureField;
use uga\hallib\ref\structure\StructureQuery;

var_dump(StructureField::getVarientList());

$structureQuery = new StructureQuery();

$structureQuery->addReturnedField('country_s');
$structureQuery->addReturnedField('name_s');

$structureQuery->baseQuery = new LiteralElement([
    'value' => 194495,
    'field' => StructureField::getVarient('docid')
]);

$executor = new OneDocQuery($structureQuery);
var_dump($executor->result);
