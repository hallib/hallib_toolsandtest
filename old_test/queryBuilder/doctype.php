<?php
namespace uga\hallibtt\test\queryBuilder;
require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\ref\doctype\DocTypeSelector;
use uga\hallib\HTMLGenerator\SelectorGenerator;

$selectorTest = new DocTypeSelector();
$selectorTest->extractData();
//uga\hallib\DocTypeSelector::updateFileData();
$selectGenerator = new SelectorGenerator($selectorTest);
$selectGenerator->name = 'doctype';
$selectGenerator->label = 'type de document';
$codeHtml = $selectGenerator->generateSelectField();
$selectGenerator->frameWorksName = 'bulma';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
    generation basic
    <?= $codeHtml ?><br><br>
    <?php $selectGenerator->name = 'doctype2'; ?>
    <?= $selectGenerator->generateSelectField() ?><br><br>
<?php
$selectorTest->portail = 'saga';
?>
    portail = saga <br><br>
    <?php $selectGenerator->name = 'doctype3'; ?>
    <?= $selectGenerator->generateSelectField() ?><br><br>
    doctype 1 + doctype 2 : <br><br>
    <?php
$selecto1 = new DocTypeSelector();
$selecto2 = new DocTypeSelector();
$selecto3 = new DocTypeSelector();
$selecto1->extractData(dirname(__FILE__).'/doctype_1.json');
$selecto2->extractData(dirname(__FILE__).'/doctype_2.json');
$selecto3->extractData(dirname(__FILE__).'/doctype_3.json');

$selecto1p2 = new DocTypeSelector();
$selecto1p2->extractData(dirname(__FILE__).'/doctype_1.json');
$selecto1p2->add($selecto2);

$selectGenerator1p2 = new SelectorGenerator($selecto1p2);
$selectGenerator1p2->name = 'doctype1p2';
$selectGenerator1p2->label = 'type de document';
$selectGenerator1p2->frameWorksName = 'bulma';
echo $selectGenerator1p2->generateSelectField();
    ?><br>
    doctype 1 - doctype 2 : <br><br>
    <?php
$selecto1m2 = new DocTypeSelector();
$selecto1m2->extractData(dirname(__FILE__).'/doctype_1.json');

$selecto1m2->minus($selecto2);

$selectGenerator1m2 = new SelectorGenerator($selecto1m2);
$selectGenerator1m2->name = 'doctype1m2';
$selectGenerator1m2->label = 'type de document';
$selectGenerator1m2->frameWorksName = 'bulma';
echo $selectGenerator1m2->generateSelectField();
    ?><br>
    intersection(doctype 1, doctype 3) : <br><br>
    <?php
$selecto1i3 = new DocTypeSelector();
$selecto1i3->extractData(dirname(__FILE__).'/doctype_1.json');

$selecto1i3->intersect($selecto3);

$selectGenerator1i3 = new SelectorGenerator($selecto1i3);
$selectGenerator1i3->name = 'doctype1i3';
$selectGenerator1i3->label = 'type de document';
$selectGenerator1i3->frameWorksName = 'bulma';
echo $selectGenerator1i3->generateSelectField();
$selectGenerator1i3->optgroupForSub = false;
echo $selectGenerator1i3->generateSelectField();
    ?>
</body>
</html>

