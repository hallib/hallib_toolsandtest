<?php
namespace uga\hallibtt\test\queryBuilder;
require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\ref\author\AuthorField;
use uga\hallib\ref\author\AuthorQuery;

var_dump(AuthorField::getVarientList());

$q = new AuthorQuery([
    'baseQuery' => new LiteralElement([
        'field' => AuthorField::getVarient('idHal_i'),
        'value' => 173671
    ]),
    'allReturnedField' => true
]);

$executor = new OneDocQuery($q);
var_dump($executor->result);

