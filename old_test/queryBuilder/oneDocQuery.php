<?php
namespace uga\hallibtt\test\queryBuilder;

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\search\SearchQuery;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

$sq = new SearchQuery();

$sq->addReturnedField('title_s');
$sq->addReturnedField('label_bibtex');
$sq->baseQuery = new LiteralElement(['value' => 'japan']);
$executor = new OneDocQuery($sq);
var_dump($executor->getReult());
