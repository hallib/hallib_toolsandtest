<?php
namespace uga\hallibtt\test\tools;

use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\QueryIterator;
use uga\hallib\ref\author\AuthorField;
use uga\hallib\ref\author\AuthorQuery;

require_once dirname(__FILE__, 4).DIRECTORY_SEPARATOR.'vendor/autoload.php';

class AuthorFieldLocal extends AuthorField {
    public static function getDataDirectory(): string {
        return dirname(__FILE__).DIRECTORY_SEPARATOR;
    }
}
AuthorFieldLocal::createList();

class AuthorQueryLocal extends AuthorQuery {
    protected static string $FieldClass = AuthorFieldLocal::class;
}

var_dump(AuthorFieldLocal::getVarient('valid_s')->valuesTranslator);

$q = new AuthorQueryLocal();
$q->baseQuery = new LiteralElement([
    'value' => '6170',
    'field' => AuthorFieldLocal::getVarient('idHal_i')
]);
$q->addReturnedField(AuthorFieldLocal::getVarient('valid_s'));

$q2 = new AuthorQueryLocal();
$q2->baseQuery = new LiteralElement([
    'value' => 'VALID',
    'field' => AuthorFieldLocal::getVarient('valid_s')
]);
$q2->addReturnedField(AuthorFieldLocal::getVarient('valid_s'));
$q2->addReturnedField(AuthorFieldLocal::getVarient('idHal_s'));
$q2->addReturnedField(AuthorFieldLocal::getVarient('label_html'));

$r1 = new OneDocQuery($q);
var_dump($r1->result);
var_dump($r1->result->valid_s);

$qit = new QueryIterator($q2);

foreach($qit as $doc) {
    var_dump($doc->label_html);
}
