<?php
namespace uga\hallibtt\test;

use uga\hallib\queryDefinition\DataContainer;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'src/queryBuildingDefine.php';

function showArray(array $ar) {
    foreach($ar as $key => $value) {
        echo 'key = '.$key.'<br>';
        echo 'value = '.$value.'<br><br>';
    }
}

class Data1 extends DataContainer {
    protected int $nombers;
    protected string $name='';
    protected array $ar = ['test'];

    public function getName() {
        return 'nom, c\'est secret <br>';
    }

    public function setName() {
        echo 'nom, je ne change pas de nom <br>';
    }
}

$d1 = [
    'name' => 'oui',
    'nombers' => '2',
    'ar' => ['non'],
];

$data1 = new Data1($d1);
showArray((new Data1($d1))->toArray());
echo (new Data1($d1))->name;
echo (new Data1($d1))->nombers;
echo '<br>';
$data1->name = 'non<br><br>';
echo var_dump($data1);
