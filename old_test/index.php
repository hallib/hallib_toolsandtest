<?php
namespace uga\hallibtt\test;

use uga\hallib\queryDefinition\DataContainer;

class A extends DataContainer {
    protected int $a = 1;
}

class B extends DataContainer {
    protected A $a;
}

$b = new B();
$b->a = new A();
var_dump($b->toArray());
$b2 = new B($b->toArray());
var_dump($b2->toArray());
