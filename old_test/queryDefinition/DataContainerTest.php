<?php declare(strict_types=1);
require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'vendor/autoload.php';
use PHPUnit\Framework\TestCase;
use uga\hallib\queryDefinition\DataContainer;

class TestData extends DataContainer {
    protected int $argent = 100;

    public function setArgent($argent) {}
}

class DataContainerTest extends TestCase {
    public function testTestData(): void
    {
        $t1 = new TestData(['argent' => 90]);
        $this->assertSame($t1->argent, 90);
        $t1->argent = 100;
        $this->assertSame($t1->argent, 90);
        $t2 = new TestData();
        $this->assertSame($t2->argent, 100);
    }
}