<?php

namespace uga\hallibtt\test\query;

use PHPUnit\Framework\TestCase;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\QueryIterator;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryElement;

require_once dirname(__FILE__, 3).DIRECTORY_SEPARATOR.'/vendor/autoload.php';

abstract class AbstractQuery extends TestCase {
    protected static string $classQuery;
    protected static string $classField;
    protected int $sampleSize = 5;
    protected array $URL_API;
    protected static string $fieldId;
    protected static string $currentFieldId;
    protected array $interval;
    protected array $filterOption;

    /**
     * Compare les résultat d'un Query Iterator avec les résultat d'une requête directe sur l'API
     *
     * @param QueryIterator $qi le Query Iterator
     * @param string $URL_API l'URL à intéroger de l'API
     * @param array $fields liste des champs à comparer
     * @param array $fieldsTranslator transformateur de com des champs renvoyer par l'API
     * @param string $source source pour le message d'erreur
     * @return void
     */
    public function cmpQIAPI(QueryIterator $qi, string $URL_API, array $fields, array $fieldsTranslator, string $source='inconnue'): void {
        $result = $this->getDocFromURL($URL_API);
        $diffURLMessage = '\n'.$URL_API.'\n'.$qi->getQuery()->stringValue;
        foreach($qi as $i => $doc) {
            foreach($fields as $fl) {
                $currentFl = isset($fieldsTranslator[$fl])?$fieldsTranslator[$fl]:$fl;
                if(isset($doc->$fl)&&isset($result[$i]->$currentFl)) {
                    $this->assertEquals($doc->$fl, $result[$i]->$currentFl, 'erreur champ '.$fl.' de '.strval($i).' '.$source.' '.$diffURLMessage);
                    if(is_array($doc->$fl)&&is_array($result[$i]->$currentFl)) {
                        $sameSize = (count($doc->$fl) === count($result[$i]->$currentFl));
                        $this->assertTrue($sameSize, 'champ '.$fl.' de '.$source.' de taille différente');
                        foreach($result[$i]->$currentFl as $idValue => $value) {
                            $this->assertEquals($doc->$fl[$idValue], $value, 'erreur champ la valeur '.$idValue.' du champs '.$fl.' de '.strval($i).' '.$source.' '.$diffURLMessage);
                        }
                    }
                }
            }
        }
    }

    public function buildQueryElement(array $confSource): QueryElement {
        if(isset($confSource['field'])&&is_string($confSource['field'])) {
            $confSource['field'] = static::$classField::getVarient($confSource['field']);
        }
        if(isset($confSource['minValue'])||isset($confSource['maxValue'])) {
            return new IntervalElement($confSource);
        } else {
            return new LiteralElement($confSource);
        }
    }

    public static function setUpBeforeClass(): void {
        static::$classField = static::$classQuery::getFieldClass();
        if(!isset(static::$currentFieldId)) {
            static::$currentFieldId = static::$fieldId;
        }
    }

    public function getDocFromURL(string $url): array {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $reponse = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->assertEquals(200, $httpCode, 'erreur response = ' . $reponse . ' url = ' . $url);
        curl_close($ch);
        $contents = json_decode($reponse);
        if(isset($contents->response)) {
            return $contents->response->docs;
        } else {
            return [];
        }
    }

    public function testSimpleLiteralQuery() {
        $query = new static::$classQuery();
        $query->rows = $this->sampleSize;
        $query->baseQuery = new LiteralElement(['value' => '*']);
        $fieldId = static::$fieldId;
        $currentFieldId = static::$currentFieldId;
        $docidField = static::$classField::getVarient(static::$fieldId);
        $query->addReturnedField($docidField);
        $query->sort = $docidField;
        $qi = new QueryIterator($query);
        $docids = [];
        foreach($qi as $doc) {
            array_push($docids, $doc->$fieldId);
        }
        $this->assertGreaterThanOrEqual(1, $qi->getNbResult(), 'API mal intéroger');
        $this->assertCount($this->sampleSize, $docids, 'nombre de résultat inférieur à $this->sampleSize');
        foreach($this->getDocFromURL($this->URL_API['simple']) as $i => $doc) {
            $this->assertEquals($doc->$currentFieldId, $docids[$i], 'element '.strval($i).' different');
        }
    }

    /**
     * test les requette de type intervale.
     * 
     * @return void
     *
     */
    public function testInterval() {
        foreach ($this->interval as $intervalName => $intervalValues) {
            $query = new static::$classQuery();
            $query->rows = $this->sampleSize;
            $query->baseQuery = $this->buildQueryElement($intervalValues['baseQuery']);
            if(isset($intervalValues['sort'])) {
                $query->sort = static::$classField::getVarient($intervalValues['sort']);
            }
            foreach($intervalValues['fl'] as $fl) {
                $query->addReturnedField($fl);
            }
            // fwrite(STDOUT, $query->stringValue . "\n");
            $qi = new QueryIterator($query);
            $fieldsTranslator = isset($intervalValues['currentFl'])?$intervalValues['currentFl']:[];
            $this->cmpQIAPI($qi, $intervalValues['URL_API'], $intervalValues['fl'], $fieldsTranslator, $intervalName);
        }
    }

    public function testCursor() {
        $query = new static::$classQuery();
        $query->useCursor = true;
        $query->rows = 1;
        $currentFieldId = static::$currentFieldId;
        $fieldId = static::$fieldId;
        $query->baseQuery = new LiteralElement(['value' => '*']);
        $docidField = static::$classField::getVarient($fieldId);
        $query->addReturnedField($docidField);
        $qi = new QueryIterator($query);
        $docs = $this->getDocFromURL($this->URL_API['cursor']);
        foreach($qi as $i => $doc) {
            if(!isset($docs[$i])) break;
            $this->assertEquals($doc->$fieldId, $docs[$i]->$currentFieldId);
        }
    }

    public function testFilter() {
        foreach($this->filterOption as $filterTestName => $filterOption) {
            $queryNoFilter = new static::$classQuery();
            $queryNoFilter->baseQuery = $this->buildQueryElement($filterOption['q']);
            $queryNoFilter->rows = 0;
            $qiNoFilter = new QueryIterator($queryNoFilter);
            $qiNoFilter->rewind();
            $query = new static::$classQuery();
            $query->baseQuery = $this->buildQueryElement($filterOption['q']);
            $query->addFilterQuery($this->buildQueryElement($filterOption['fq']));
            $query->rows = $this->sampleSize;
            $query->sort = static::$classField::getVarient($filterOption['sort']);
            foreach($filterOption['fl'] as $fl) {
                $query->addReturnedField($fl);
            }
            $qi = new QueryIterator($query);
            $fieldsTranslator = isset($filterOption['currentFl'])?$filterOption['currentFl']:[];
            $this->cmpQIAPI($qi, $filterOption['URL_API'], $filterOption['fl'], $fieldsTranslator, $filterTestName);
            $this->assertNotEquals($qi->getNbResult(), $qiNoFilter->getNbResult());
        }
    }
}
