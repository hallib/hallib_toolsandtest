<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\journal\JournalQuery;
use uga\hallibtt\test\query\AbstractQuery;

class JournalQueryTest extends AbstractQuery {
    protected static string $classQuery = JournalQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/journal?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/journal?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'title' => [
            'baseQuery' => [
                'field' => 'title_s',
                'minValue' => '"aa"',
                'maxValue' => '"ae"',
            ],
            'sort' => 'title_s',
            'fl' => ['docid', 'title_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/journal?q=title_s:[%22aa%22%20TO%20%22ae%22]&sort=title_s%20desc&fl=docid,title_s&rows=5',
        ]
    ];

    /**
     * @doesNotPerformAssertions
     */
    public function testFilter() {}
}
