<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\structure\StructureQuery;
use uga\hallibtt\test\query\AbstractQuery;

class StructureQueryTest extends AbstractQuery {
    protected static string $classQuery = StructureQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/structure?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/structure?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'acronym' => [
            'baseQuery' => [
                'field' => 'acronym_s',
                'minValue' => 'a',
                'maxValue' => 'c',
            ],
            'sort' => 'docid',
            'fl' => ['docid', 'acronym_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/structure?q=acronym_s:[a%20TO%20c]&sort=docid%20desc&fl=docid,acronym_s&rows=5'
        ]
    ];

    /**
     * @doesNotPerformAssertions
     */
    public function testFilter() {}
}
