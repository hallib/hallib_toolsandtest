<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\domain\DomainQuery;
use uga\hallibtt\test\query\AbstractQuery;

class DomainQueryTest extends AbstractQuery {
    protected static string $classQuery = DomainQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/domain?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/domain?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'date' => [
            'baseQuery' => [
                'field' => 'docid',
                'minValue' => '0',
                'maxValue' => '50',
            ],
            'sort' => 'docid',
            'fl' => ['docid', 'code_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/domain?q=docid:[0%20TO%2050]&sort=docid%20desc&fl=docid,code_s&rows=5',
        ]
    ];
    protected array $filterOption = [
        'submitted firstName_s' => [
            'q' => [
                'field' => 'docid',
                'minValue' => '0',
                'maxValue' => '50',
            ],
            'fq' => [
                'field' => 'text',
                'value' => 'd'
            ],
            'fl' => ['docid'],
            'sort' => 'docid',
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/domain?q=docid:[0%20TO%2050]&rows=5&fl=docid,code_s&fq=text:d&fl=docid&sort=docid%20desc'
        ]
    ];
}
