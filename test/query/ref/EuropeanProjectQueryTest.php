<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\europeanproject\EuropeanProjectQuery;
use uga\hallibtt\test\query\AbstractQuery;

class EuropeanProjectQueryTest extends AbstractQuery {
    protected static string $classQuery = EuropeanProjectQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/europeanproject?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/europeanproject?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'title' => [
            'baseQuery' => [
                'field' => 'title_s',
                'minValue' => '"aa"',
                'maxValue' => '"ae"',
            ],
            'sort' => 'title_s',
            'fl' => ['docid', 'title_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/europeanproject?q=title_s:[%22aa%22%20TO%20%22ae%22]&sort=title_s%20desc&fl=docid,title_s&rows=5',
        ]
    ];
    protected array $filterOption = [
        'submitted firstName_s' => [
            'q' => [
                'field' => 'acronym_s',
                'minValue' => 'a',
                'maxValue' => 'm',
            ],
            'fq' => [
                'field' => 'text',
                'value' => 'd'
            ],
            'fl' => ['docid', 'acronym_s'],
            'sort' => 'docid',
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/europeanproject?q=acronym_s:[a%20TO%20m]&rows=5&fl=docid,acronym_s&fq=text:d&sort=docid%20desc'
        ]
    ];
}
