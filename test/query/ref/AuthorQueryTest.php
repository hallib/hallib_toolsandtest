<?php
namespace uga\hallibtt\test\queryBuilder\ref;

use uga\hallib\ref\author\AuthorQuery;
use uga\hallibtt\test\query\AbstractQuery;

class AuthorQueryTest extends AbstractQuery {
    protected static string $classQuery = AuthorQuery::class;
    protected array $URL_API = [
        'simple' => 'https://api.archives-ouvertes.fr/ref/author?q=*&rows=5&fl=docid&sort=docid%20desc',
        'cursor' => 'https://api.archives-ouvertes.fr/ref/author?q=*&rows=5&fl=docid&sort=docid%20asc',
    ];
    protected static string $fieldId = 'docid';
    protected array $interval = [
        'idhal' => [
            'baseQuery' => [
                'field' => 'idHal_i',
                'minValue' => '170023',
                'maxValue' => '1162851',
            ],
            'sort' => 'firstName_s',
            'fl' => ['docid', 'firstName_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/author?q=idHal_i:[170023%20TO%201162851]&sort=firstName_s%20desc&fl=docid,abstract_s&rows=5',
        ]
    ];
    protected array $filterOption = [
        'submitted firstName_s' => [
            'q' => [
                'field' => 'firstName_s',
                'minValue' => 'A',
                'maxValue' => 'C',
            ],
            'fq' => [
                'field' => 'text',
                'value' => 'd'
            ],
            'sort' => 'fullName_s',
            'fl' => ['docid', 'fullName_s'],
            'URL_API' => 'https://api.archives-ouvertes.fr/ref/author?q=firstName_s:[A%20TO%20C]&sort=fullName_s%20desc&fl=docid,fullName_s&fq=text:d',
        ]
    ];
}
